#ifndef REGORLOG_H
#define REGORLOG_H

#include <QDialog>
#include "user.h"
namespace Ui {
class regORlog;
}

class regORlog : public QDialog
{
    Q_OBJECT
    bool log_=false;

public:
    explicit regORlog(QWidget *parent = nullptr);
    ~regORlog();
    bool data();
signals:
    void signalFromButton(bool log);
private slots:
    void set(bool log);
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::regORlog *ui;
};

#endif // REGORLOG_H

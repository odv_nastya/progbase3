#ifndef ADDDIALOGGDP_H
#define ADDDIALOGGDP_H
#include "gdp.h"
#include <QDialog>

namespace Ui {
class AddDialogGDP;
}

class AddDialogGDP : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialogGDP(QWidget *parent = nullptr);
    ~AddDialogGDP();
    GDP data();

private:
    Ui::AddDialogGDP *ui;
};

#endif // ADDDIALOGGDP_H

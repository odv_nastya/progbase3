#ifndef ALBUM_H
#define ALBUM_H

#include<string>
#include <iostream>
#include <QListWidgetItem>
using namespace std;
using namespace std;

struct GDP
{

    int id;
    int gdp;
    int gdpPer;
    int unemp;
    GDP(){}
    GDP(const GDP & al){this->id=al.id;this->gdp=al.gdp;this->gdpPer=al.gdpPer;this->unemp=al.unemp;}
};

Q_DECLARE_METATYPE(GDP);
#endif // ALBUM_H

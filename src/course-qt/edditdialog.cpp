#include "edditdialog.h"
#include "ui_edditdialog.h"
#include <QFile>
#include <QFileDialog>
EdditDialog::EdditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EdditDialog)
{
    ui->setupUi(this);
}
void EdditDialog::setData(const capital & cat)
{
    ui->lineEdit->setText(QString::fromStdString( cat.continent));
    ui->lineEdit_2->setText(QString::fromStdString( cat.Thecountry));
    ui->lineEdit_3->setText(QString::fromStdString( cat.capital_));
    ui->spinBox->setValue(( cat.population));
//    ui->spinBox->setValue(cat.age);
//    ui->doubleSpinBox->setValue(cat.weight);
    ui->lineEdit_4->setText(QString::fromStdString(cat.photo));
}
capital EdditDialog::data()
{
    capital cat;
    cat.continent=ui->lineEdit->text().toStdString();
    cat.population=ui->spinBox->value();
    cat.Thecountry=ui->lineEdit_2->text().toStdString();
    cat.capital_=ui->lineEdit_3->text().toStdString();
//    cat.weight=ui->doubleSpinBox->value();
    QFile file(ui->lineEdit_4->text());
    file.rename(QString::fromStdString("/home/nastya-odv/progbase3/photo/"+cat.Thecountry+".jpg"));
    cat.photo="/home/nastya-odv/progbase3/photo/"+cat.Thecountry+".jpg";
    return cat;
}
EdditDialog::~EdditDialog()
{
    delete ui;
}

void EdditDialog::on_pushButton_clicked()
{
    ui->lineEdit_4->setText(QFileDialog::getOpenFileName(this,"Dialog Caption","","JPEG (*.jpg)"));
}

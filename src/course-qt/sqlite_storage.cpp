#include "sqlite_storage.h"
#include <QDebug>
#include <QCryptographicHash>
using namespace std;
SqliteStorage::SqliteStorage(const string &dir_name): Storage(dir_name)
{
db =QSqlDatabase ::addDatabase("QSQLITE");
}
static QString hashPassword(QString const & pass)
{
    QByteArray pass_ba=pass.toUtf8();
    pass_ba=QCryptographicHash::hash(pass_ba,QCryptographicHash::Md5);
    QString pass_hash=QString(pass_ba.toHex());
    return pass_hash;
}
capital getCountryFromQuery(const QSqlQuery & query)
{
    int id=query.value("id").toInt();
    string continent=(query.value("continent").toString().toStdString());
    string Thecountry=query.value("country").toString().toStdString();
    string capital_=query.value("capital").toString().toStdString();
    string photo=query.value("photo").toString().toStdString();
    int population=query.value("population").toInt();
    int user_id=query.value("users_id").toInt();
    capital Country;
    Country.id=id;
    Country.Thecountry=Thecountry;
    Country.continent=continent;
    Country.capital_=capital_;
    Country.population=population;
    Country.photo=photo;
    Country.user_id=user_id;
    return Country;
}
GDP getGDPFromQuery(const QSqlQuery & query)
{

    int id=query.value("id").toInt();
    int gdp=(query.value("GDPbillion").toInt());
    int gdpper=query.value("GDPperperson").toInt();
    int un=query.value("unemp").toInt();
    GDP GDP_;
    GDP_.id=id;
    GDP_.gdp=gdp;
    GDP_.gdpPer=gdpper;
    GDP_.unemp=un;
    return  GDP_;
}
bool SqliteStorage::isOpen() const
{
    return db.isOpen();
}
bool SqliteStorage::open()
{
   QString path =QString::fromStdString(this->name());
    db.setDatabaseName(path);
    if(!db.open())
        return false;
     return true;
}
void SqliteStorage::close()
{
    db.close();
}
vector<capital> SqliteStorage::getAllCountry()
{

    vector<capital> C;
    this->open();
    QSqlQuery query("SELECT * FROM capitals");

    while (query.next())
    {
        qDebug()<<"qq";
      capital Country= getCountryFromQuery(query);
        C.push_back(Country);
        qDebug()<<QString::fromStdString( Country.photo);
    }
    this->close();
    return C;
}
optional<capital> SqliteStorage::getCountryById(int country_id)
{
    this->open();
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM capitals WHERE id = :id"))
    {
       qDebug() << "get person query prepare error:" ;
       // return or throw or do smth else
        return nullopt;
    }
    query.bindValue(":id", country_id);
    if (!query.exec())
    {  // do exec if query is prepared SELECT query
       qDebug() << "get person query exec error:" ;
       // return or throw or do smth else
       return nullopt;
    }
    if (query.next())
    {
        capital Country= getCountryFromQuery(query);
        return Country;
    }
    else
    {
        cout<<"wt";
       qDebug() << " not found ";
       return nullopt;
    }
    this->close();

}
bool SqliteStorage::updateCountry(const capital & country)
{
    this->open();
    QSqlQuery query;
    if (!query.prepare("UPDATE capitals SET continent = :continent, country=:country, capital=:capital, population=:population,photo=:photo, users_id=:users_id WHERE id = :id")){
        qDebug() << "updatePerson query prepare error:" ;
        // return or throw or do smth else
    }
    query.bindValue(":continent",QString::fromStdString( country.continent));
    query.bindValue(":country",QString::fromStdString( country.Thecountry));
    query.bindValue(":capital",QString::fromStdString( country.capital_));
    query.bindValue(":photo",QString::fromStdString( country.photo));
    query.bindValue(":population",( country.population));
    query.bindValue(":id", country.id);
    query.bindValue(":users_id",country.user_id);
    if (!query.exec()){
        qDebug() << "updatePerson query exec error:" ;
        // return or throw or do smth else
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    this->close();
    return true;
}
bool SqliteStorage::removeCountry(int country_id)
{
    this->open();
    QSqlQuery query;
    if (!query.prepare("DELETE FROM capitals WHERE id = :id")){
        qDebug() << "deletePerson query prepare error:" ;
        // return or throw or do smth else
    }
    query.bindValue(":id", country_id);
    if (!query.exec()){
        qDebug() << "deletePerson query exec error:" ;
        // return or throw or do smth else
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
    this->close();
}
int SqliteStorage::insertCountry(const capital & country)
{
    this->close();
    this->open();
    QSqlQuery query;
    query.prepare("INSERT INTO capitals ( continent, country, capital, population,users_id,photo) VALUES ( :continent, :country, :capital, :population,:users_id,:photo)");
    query.bindValue(":continent", QString::fromStdString(country.continent));
    query.bindValue(":country", QString::fromStdString(country.Thecountry));
    query.bindValue(":capital", QString::fromStdString(country.capital_));
    query.bindValue(":photo", QString::fromStdString(country.photo));
    query.bindValue(":population", (country.population));
    query.bindValue(":users_id", (country.user_id));


    if (!query.exec())
    {
        cout<<query.exec();
        return  0;
    }
    QVariant var=query.lastInsertId();
    this->close();
    return var.toInt();

}
int SqliteStorage::insertUser(const User & country)
{
    this->close();
    this->open();
    QSqlQuery query;
    query.prepare("INSERT INTO users ( username, password_hash) VALUES ( :username, :password_hash)");
    query.bindValue(":username", (country.username));
    QString a=hashPassword((country.hash_password));
    query.bindValue(":password_hash", a);
    if (!query.exec())
    {
        cout<<query.exec();
        return  0;
    }
    QVariant var=query.lastInsertId();
    this->close();
    return var.toInt();

}
vector<GDP> SqliteStorage::getAllGDP()
{
    vector<GDP> G;
    this->open();
    QSqlQuery query("SELECT * FROM GPDs");

    while (query.next())
    {
        GDP GDP_= getGDPFromQuery(query);
        G.push_back(GDP_);
    }
    this->close();
    return G;
}
optional<GDP> SqliteStorage::getGDPById(int gdp_id)
{

    this->open();
    QSqlQuery query;
    if (!query.prepare("SELECT * FROM GPDs WHERE id = :id"))
    {
       qDebug() << "get person query prepare error:" ;
       // return or throw or do smth else
        return nullopt;
    }
    query.bindValue(":id", gdp_id);
    if (!query.exec())
    {  // do exec if query is prepared SELECT query
       qDebug() << "get person query exec error:" ;
       // return or throw or do smth else
       return nullopt;
    }
    if (query.next())
    {
        GDP GDP_= getGDPFromQuery(query);
        return GDP_;
    }
    else
    {
        cout<<"wt";
       qDebug() << " not found ";
       return nullopt;
    }
    this->close();
}
bool SqliteStorage::updateGDP(const GDP & gdp)
{
    this->open();
    QSqlQuery query;
    if (!query.prepare("UPDATE GPDs SET GDPbillion = :GDPbillion, GDPperperson=:GDPperperson, unemp=:unemp WHERE id = :id")){
        qDebug() << "updatePerson query prepare error:" ;
        // return or throw or do smth else
    }
    query.bindValue(":GDPbillion",( gdp.gdp));
    query.bindValue(":GDPperperson",( gdp.gdpPer));
    query.bindValue(":unemp",( gdp.unemp));
    query.bindValue(":id", gdp.id);
    if (!query.exec()){
        qDebug() << "updatePerson query exec error:" ;
        // return or throw or do smth else
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    this->close();
    return true;
}
bool SqliteStorage::removeGDP(int gdp_id)
{
    this->open();
    QSqlQuery query;
    if (!query.prepare("DELETE FROM GPDs WHERE id = :id")){
        qDebug() << "deletePerson query prepare error:" ;
        // return or throw or do smth else
    }
    query.bindValue(":id", gdp_id);
    if (!query.exec()){
        qDebug() << "deletePerson query exec error:" ;
        // return or throw or do smth else
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }
    return true;
    this->close();

}
int SqliteStorage::insertGDP(const GDP & gdp)
{

    this->open();


    QSqlQuery query;
     query.prepare("INSERT INTO GPDs ( GDPbillion, GDPperperson, unemp) VALUES ( :GDPbillion, :GDPperperson, :unemp)");
    query.bindValue(":GDPbillion",( gdp.gdp));
    query.bindValue(":GDPperperson",(gdp.gdpPer));
    query.bindValue(":unemp",( gdp.unemp));

    if (!query.exec())
    {
       qDebug() << "addPerson qu:";
       return 0;
    }
    QVariant var=query.lastInsertId();
    this->close();
    return var.toInt();

}

optional<User> SqliteStorage::getUserAuth( QString const & username, QString const & password)
{
    qDebug()<<"Hello";
    if(!this->open())
    {

        throw "Database can`t be open";
     }
  QSqlQuery query;
  if(!query.prepare("SELECT * FROM users WHERE username=:username AND password_hash=:password_hash;"))
  {
      QSqlError error=query.lastError();
      throw error;
  }
  query.bindValue(":username",username);
  query.bindValue(":password_hash",hashPassword(password));
  if(!query.exec())
  {
     QSqlError error =query.lastError();
     throw error;
  }
  if(query.next())
  {
      User user;
      user.id =query.value("id").toInt();
      user.username=username;
      user.hash_password=query.value("password_hash").toString();
      //user.fullname=query.value("fullname").toString();
      this->close();
      return user;
  }
  this->close();
  return nullopt;
}
vector<capital> SqliteStorage::getAllUserCountry(int user_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    if(!query.prepare("SELECT * FROM capitals WHERE users_id=:users_id"))
    {
        QSqlError error=query.lastError();
        throw error;
    }
    query.bindValue(":users_id",user_id);
    if(!query.exec())
    {
       QSqlError error =query.lastError();
       throw error;
    }
    vector<capital> cats;
    while(query.next())
    {
        capital c;
        c.id=query.value(0).toInt();
        c.continent=query.value(1).toString().toStdString();
        c.Thecountry=query.value(2).toString().toStdString();
        c.capital_=query.value(3).toString().toStdString();
        c.population=query.value(4).toInt();
        c.photo=query.value(6).toString().toStdString();
        c.user_id=query.value(5).toInt();
        qDebug()<<c.user_id;
        cats.push_back(c);
    }
    this->close();
    return cats;

}
vector<GDP> SqliteStorage::getAllCountryGDPs(int country_id)
{
    this->open();
 vector<GDP> subjects;
    QSqlQuery query;
    query.prepare("select * from GPDs where id in (select gpd_id from links where country_id = :country_id)");
    query.bindValue(":country_id", country_id);
    if(!query.exec()){
        qDebug() << "Can`t exec query";

    }
    while(query.next()){
        GDP subject;
        subject.id = query.value("id").toInt();
        subject.gdp = query.value("GDPbillion").toInt();;
        subject.gdpPer = query.value("GDPperperson").toInt();
        subject.unemp = query.value("unemp").toInt();;
        subjects.push_back(subject);
    }
    this->close();
    return subjects;
}
bool SqliteStorage::insertCountryGDPs(int country_id,int gdp_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("INSERT INTO links (country_id,gpd_id) VALUES (:country_id,:gpd_id);");
    query.bindValue(":country_id",country_id);
    query.bindValue(":gpd_id", gdp_id);
    if(!query.exec())
    {
        QSqlError error =query.lastError();
        throw error;
        this->close();
        return false;
    }
    this->close();
    return true;
}
bool SqliteStorage::removeCountryGDP(int country_id,int gdp_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE country_id=:country_id AND gpd_id=:gpd_id;");
    query.bindValue(":country_id",country_id);
    query.bindValue(":gpd_id", gdp_id);
    if(!query.exec())
    {
        QSqlError error =query.lastError();
        throw error;
        this->close();
        return false;
    }
    this->close();
    return true;
}

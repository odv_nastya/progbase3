#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "user.h"
namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();
    User data();

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H

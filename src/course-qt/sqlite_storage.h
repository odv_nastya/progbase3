#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H
#include <iostream>
#include<fstream>
#include <optional>
#include <string>
#include <storage.h>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include "user.h"
using namespace std;

class SqliteStorage:public Storage
{
protected:
    QSqlDatabase db;
public:
    SqliteStorage(const string &dir_name);
    bool isOpen() const;
    bool open();
    void close();

    vector<capital> getAllCountry();
    optional<capital> getCountryById(int country_id);
    bool updateCountry(const capital & country);
    bool removeCountry(int country_id);
    int insertCountry(const capital & country);

    vector<GDP> getAllGDP();
    optional<GDP> getGDPById(int gdp_id);
    bool updateGDP(const GDP & gdp);
    bool removeGDP(int gdp_id);
    int insertGDP(const GDP & gdp);
    void csv_to_table(vector<capital> a);
    optional<User> getUserAuth(QString const & username, QString const & password);
    vector<capital> getAllUserCountry(int user_id);
    int insertUser(const User & country);
    vector<GDP> getAllCountryGDPs(int country_id);
    bool insertCountryGDPs(int country_id,int gdp_id);
    bool removeCountryGDP(int country_id,int gdp_id);
};

#endif // SQLITE_STORAGE_H

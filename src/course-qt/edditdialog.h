#ifndef EDDITDIALOG_H
#define EDDITDIALOG_H

#include <QDialog>
#include"capital.h"
namespace Ui {
class EdditDialog;
}

class EdditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EdditDialog(QWidget *parent = 0);
    ~EdditDialog();
     capital data();
     void setData(const capital & ph);
private slots:
     void on_pushButton_clicked();

private:
    Ui::EdditDialog *ui;
};

#endif // EDDITDIALOG_H

#ifndef AUTH_H
#define AUTH_H

#include <QDialog>
#include "user.h"
namespace Ui {
class Auth;
}

class Auth : public QDialog
{
    Q_OBJECT

public:
    explicit Auth(QWidget *parent = 0);
    User data();

    User dataNew();
    ~Auth();

private slots:
     void on_pushButton_clicked();

private:
    Ui::Auth *ui;
};

#endif // AUTH_H

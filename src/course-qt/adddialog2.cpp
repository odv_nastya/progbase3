#include "adddialog2.h"
#include "ui_adddialog2.h"
#include <QDebug>
adddialog2::adddialog2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::adddialog2)
{
    ui->setupUi(this);
}
GDP adddialog2::data()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    GDP al;
    foreach(QListWidgetItem *item,items)
    {
        al=item->data(Qt::UserRole).value<GDP>();
   }
  return al;
}
void adddialog2::setData(vector<GDP> al)
{
    for(const GDP& album:al)
    {
        QVariant qVar=QVariant::fromValue(album);
        QListWidgetItem * qAlbumListItem=new QListWidgetItem();
        qAlbumListItem->setText(QString::number(album.unemp));
        qAlbumListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qAlbumListItem);
    }
}
adddialog2::~adddialog2()
{
    delete ui;
}

void adddialog2::on_listWidget_itemClicked(QListWidgetItem *item)
{
  ui->buttonBox->setEnabled(true);
}

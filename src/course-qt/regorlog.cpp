#include "regorlog.h"
#include "ui_regorlog.h"
#include "mainwindow.h"
#include "sqlite_storage.h"
#include "auth.h"
regORlog::regORlog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::regORlog)
{
    ui->setupUi(this);
    connect(this,&regORlog::signalFromButton,this,&regORlog::set);
}

regORlog::~regORlog()
{
    delete ui;
}
bool regORlog::data()
{
    return log_;
}

void regORlog::on_pushButton_clicked()
{

    emit signalFromButton(false);
}

void regORlog::on_pushButton_2_clicked()
{
    emit signalFromButton(true);
}
void regORlog::set(bool log)
{
    log_=log;
    accept();
}

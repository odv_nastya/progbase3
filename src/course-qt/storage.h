#ifndef STORAGE_H
#define STORAGE_H

#include<vector>
#include "capital.h"
#include "gdp.h"
#include"user.h"
#include <optional>
using namespace  std;

class Storage
{
private:
    string dir_name_;
public:
    Storage(){}
    explicit Storage(const string & dir_name=""){dir_name_ = dir_name;}
    virtual ~Storage(){}
    void setName(const string & dir_name);
    string name() const;

     virtual bool isOpen() const=0;
    virtual bool open()=0;
     virtual void close()=0;

     virtual vector<capital> getAllCountry()=0;
     virtual optional<capital> getCountryById(int cat_id)=0;
     virtual bool updateCountry(const capital & cat)=0;
     virtual bool removeCountry(int cat_id)=0;
     virtual int insertCountry(const capital & cat)=0;

     virtual vector<GDP> getAllGDP()=0;
     virtual optional<GDP> getGDPById(int breed_id)=0;
     virtual bool updateGDP(const GDP & breed)=0;
     virtual bool removeGDP(int breed_id)=0;
     virtual int insertGDP(const GDP & breed)=0;

     virtual optional<User> getUserAuth(QString const & username, QString const & password)=0;
    virtual vector<capital> getAllUserCountry(int user_id)=0;
    virtual int insertUser(const User & cat)=0;
    virtual vector<GDP> getAllCountryGDPs(int cat_id)=0;
    virtual bool insertCountryGDPs(int cat_id,int breed_id)=0;
    virtual bool removeCountryGDP(int cat_id,int breed_id)=0;
};

#endif // STORAGE_H

#include "adddialog.h"
#include "ui_adddialog.h"
#include <QFile>
#include <QFileDialog>
#include <QDebug>
using namespace std;
AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}

capital AddDialog::data()
{
    capital cat;


    cat.continent =ui->lineEdit->text().toStdString();
    cat.population=ui->spinBox->value();
    cat.Thecountry=ui->lineEdit_2->text().toStdString();
    cat.capital_=ui->lineEdit_3->text().toStdString();
    QFile file(ui->lineEdit_4->text());
    file.copy(QString::fromStdString("/home/nastya-odv/progbase3/photo/"+cat.Thecountry+".jpg"));
    cat.photo="/home/nastya-odv/progbase3/photo/"+cat.Thecountry+".jpg";
    return cat;
}
AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::on_pushButton_clicked()
{
    ui->lineEdit_4->setText(QFileDialog::getOpenFileName(this,"Dialog Caption","","JPEG (*.jpg)"));
}

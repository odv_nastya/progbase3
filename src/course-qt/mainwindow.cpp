#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include "adddialog.h"
#include "edditdialog.h"
#include <QMessageBox>
#include "sqlite_storage.h"
#include<QDebug>
#include "adddialog2.h"
#include "auth.h"
#include "adddialoggdp.h"
#include "regorlog.h"
#include"dialog.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    QPixmap pix("../camera.png");
//    ui->label->setPixmap(pix);
    connect(ui->actionOpen,&QAction::triggered,this,&MainWindow::onOpen);
    connect(ui->actionExit,&QAction::triggered,this,&MainWindow::beforeClose);
    connect(ui->actionlogout,&QAction::triggered,this,&MainWindow::onLogout);
    ui->add_new->setEnabled(false);
}

MainWindow::~MainWindow()
{
    if(storage_!=nullptr)
        delete storage_;
    delete ui;
}
void MainWindow::addCats(const vector<capital> & countries)
{
    for(const capital & country:countries)
    {
        QVariant qVar=QVariant::fromValue(country);
        QListWidgetItem * qCatListItem=new QListWidgetItem();
        qCatListItem->setText(QString::fromStdString(country.Thecountry));
        qCatListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qCatListItem);
    }



}
void MainWindow::onOpen()
{
    QString fileName=QFileDialog::getOpenFileName(this,"Dialog Caption","","SQLITE (*.sqlite)");
    if(!fileName.isEmpty()&&!fileName.isNull())
    {
        if(storage_!=nullptr)
        {
            delete storage_;
            ui->listWidget->clear();
            ui->listWidget_2->clear();
        }
        storage_=new SqliteStorage(fileName.toStdString());
        if(!storage_->open())
        {
            cout<<"ss";
        }
        ui->label_2->setText("");
        ui->selected->setText("");
        ui->selected_2->setText("");
        ui->remove_button->setEnabled(false);
        ui->edit_button->setEnabled(false);
        ui->remove_button_2->setEnabled(false);
        ui->add_button_2->setEnabled(false);
        ui->add_button->setEnabled(false);
        ui->add_new->setEnabled(false);
//         User user;
//        Auth auth(this);
//        //regORlog a(this);

//         if(auth.exec())
//        {
//             user=auth.data();
//         }
//        else
//        {
//            qDebug()<<"1";
//           return;
//        }
//        //qDebug()<<user.username<<user.hash_password;
//        const User u=user;

//        while (storage_->getUserAuth(user.username,user.hash_password)==nullopt)
//        {
//            qDebug()<<user.username;
//            QMessageBox::information(this,"Authentication","Incorrect login or password. Try again");
//            Auth auth2(this);
//            if(auth2.exec())
//            {
//                user=auth2.data();
//            }
//            else
//            {
//                qDebug()<<"2";
//                return;
//            }
//        }

//        user_=storage_->getUserAuth(user.username,user.hash_password).value();
//        ui->add_button->setEnabled(true);
//        //ui->label_2->setText(user_.fullname);
//        qDebug()<<fileName;
//        vector<capital> b=storage_->getAllUserCountry(user_.id);
//        qDebug()<<"Size: "<<b.size()<<"User:"<<user_.id;
//        this->addCats(b);
        this->enter();



    }

    else
    {

    }
}

void MainWindow::addBreeds(const vector<GDP> & GDPs)
{

    for(const GDP & gdp:GDPs)
    {
        QVariant qVar=QVariant::fromValue(gdp);
        QListWidgetItem * qALbumListItem=new QListWidgetItem();
        qALbumListItem->setText(QString::number(gdp.unemp));
        qALbumListItem->setData(Qt::UserRole,qVar);
        ui->listWidget_2->addItem(qALbumListItem);
    }
}
void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
        ui->listWidget_2->clear();
        ui->selected_2->setText("");
        ui->remove_button->setEnabled(true);
        ui->edit_button->setEnabled(true);
        ui->remove_button_2->setEnabled(false);
        ui->add_button_2->setEnabled(true);
        ui->add_new->setEnabled(true);
    capital country= item->data(Qt::UserRole).value<capital>();
    vector<GDP> gdps=storage_->getAllCountryGDPs(country.id);
    qDebug()<<gdps.size();
    this->addBreeds(gdps);
    ui->selected->setText("Selected Country:\nId:"+QString::number(country.id)+
                          "\nname:"+ QString::fromStdString(country.Thecountry)+
                          "\nCapital:"+QString::fromStdString(country.capital_)+" \npopulation:"+QString::number(country.population));
    QPixmap pix(QString::fromStdString(country.photo));
    qDebug()<<QString::fromStdString( country.photo);
    ui->label->setPixmap(pix);
}

void MainWindow::on_remove_button_clicked()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On remove","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {
        if(ui->listWidget->selectedItems().isEmpty())
        {
            ui->add_button_2->setEnabled(false);
            ui->selected->setText("");
            ui->label->setText("");
           ui->remove_button->setEnabled(false);
           ui->edit_button->setEnabled(false);
        }
        ui->selected_2->setText("");
        QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
        capital country;
        foreach(QListWidgetItem *item,items)
        {
            country=item->data(Qt::UserRole).value<capital>();
            storage_->removeCountry(country.id);
            delete ui->listWidget->takeItem(ui->listWidget->row(item));
        }
        items=ui->listWidget->selectedItems();
        if(items.size()!=0)
        {
            country=items[0]->data(Qt::UserRole).value<capital>();
            ui->listWidget_2->clear();
            ui->remove_button_2->setEnabled(false);
            vector<GDP> gdps=storage_->getAllCountryGDPs(country.id);
            this->addBreeds(gdps);
        }
        else
        {
            ui->listWidget_2->clear();
            ui->label_2->setText("");
            ui->add_button_2->setEnabled(false);
            ui->remove_button_2->setEnabled(false);
        }

    }
}

void MainWindow::on_add_button_clicked()
{

    AddDialog add(this);
    int status=add.exec();
    if(status==1)
    {
        capital country=add.data();
        country.user_id=user_.id;
        country.id=storage_->insertCountry(country);
        QVariant qVar=QVariant::fromValue(country);
        QListWidgetItem * qCatListItem=new QListWidgetItem();
        qCatListItem->setText(QString::fromStdString(country.Thecountry));
        qCatListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qCatListItem);
    }
    else
    {

    }
}

void MainWindow::on_edit_button_clicked()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    capital country;
        foreach(QListWidgetItem *item,items)
        {
            country=item->data(Qt::UserRole).value<capital>();
            EdditDialog edit(this);
            edit.setData(country);
            int status=edit.exec();
            if(status==1)
            {
                capital c=edit.data();
                c.id=country.id;
                storage_->updateCountry(c);
                QVariant qVar=QVariant::fromValue(c);
                item->setData(Qt::UserRole,qVar);
                item->setText(QString::fromStdString(c.Thecountry));
                ui->listWidget->editItem(item);
                ui->selected->setText("Selected Country:\nId:"+QString::number(country.id)+
                                      "\nname:"+ QString::fromStdString(country.Thecountry)+
                                      "\nCapital:"+QString::fromStdString(country.capital_)+" \npopulation:"+QString::number(country.population));
                QPixmap pix(QString::fromStdString(country.photo));
                ui->label->setPixmap(pix);
            }

        }
}
void MainWindow::beforeClose()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On exit","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {
        this->close();
    }
}

void MainWindow::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    ui->remove_button_2->setEnabled(true);
GDP al= item->data(Qt::UserRole).value<GDP>();
ui->selected_2->setText("Selected GDP:\nId:"+QString::number(al.id)+
                      "\nGDP billion:"+ QString::number(al.gdp)+
                      "\nGDP per person:"+QString::number(al.gdpPer)+"\nyere:"+QString::number(al.unemp)+"years");
}

void MainWindow::on_add_button_2_clicked()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    capital cat;
    foreach(QListWidgetItem *item,items)
    {
        cat=item->data(Qt::UserRole).value<capital>();
     }
    vector<GDP> breeds;
    vector<GDP> all_al=storage_->getAllGDP();
    vector<GDP> cat_al=storage_->getAllCountryGDPs(cat.id);
    for(int i=0;i<all_al.size();i++)
    {
        bool exist=false;
        for(int j=0;j<cat_al.size();j++)
        {
            if(all_al[i].id==cat_al[j].id)
            {

                exist=true;
                break;
            }
        }
        if(!exist)
        {
            qDebug()<<"sa";
            breeds.push_back(all_al[i]);
        }
    }
    adddialog2 add(this);
    add.setData(breeds);
    int status=add.exec();
    if(status==1)
    {
        GDP breed=add.data();
        storage_->insertCountryGDPs(cat.id,breed.id);
        QVariant qVar=QVariant::fromValue(breed);
        QListWidgetItem * qBreedListItem=new QListWidgetItem();
        qBreedListItem->setText(QString::number(breed.unemp));
        qBreedListItem->setData(Qt::UserRole,qVar);
        ui->listWidget_2->addItem(qBreedListItem);
    }
    else
    {

    }
}

void MainWindow::on_remove_button_2_clicked()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On remove","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {

        QList <QListWidgetItem*> items=ui->listWidget_2->selectedItems();
        GDP al;
        al=items[0]->data(Qt::UserRole).value<GDP>();
        QList <QListWidgetItem*> items2=ui->listWidget->selectedItems();
        capital cat=items2[0]->data(Qt::UserRole).value<capital>();
        qDebug()<<al.id<<"|"<<cat.id;
        try
        {storage_->removeCountryGDP(cat.id,al.id);}
        catch(string i)
        {
            cout<<i;
        }
        catch(QSqlError error)
        {
            qDebug()<<error.text();
        }
        delete ui->listWidget_2->takeItem(ui->listWidget_2->row(items[0]));
        if(ui->listWidget_2->selectedItems().isEmpty())
        {
            ui->add_button_2->setEnabled(false);
            ui->remove_button_2->setEnabled(false);
            ui->selected_2->setText("");
        }
    }
}
void MainWindow::onLogout()
{
 user_.id=0;
 //user_.fullname="";
 user_.hash_password="";
 user_.username="";
 ui->label_2->setText("");
 ui->selected->setText("");
 ui->label->setText("");
 ui->selected_2->setText("");
 ui->remove_button->setEnabled(false);
 ui->edit_button->setEnabled(false);
 ui->remove_button_2->setEnabled(false);
 ui->add_button_2->setEnabled(false);
 ui->add_button->setEnabled(false);
 ui->listWidget->clear();
 ui->listWidget_2->clear();
 User user;
Auth auth(this);
if(auth.exec())
{
    user=auth.data();
}
else
{
   return;
}
qDebug()<<user.username<<user.hash_password;
const User u=user;
while (storage_->getUserAuth(user.username,user.hash_password)==nullopt)
{
    qDebug()<<user.username;
    QMessageBox::warning(this,"Authentication","Incorrect login or password. Try again");
    Auth auth2(this);
    if(auth2.exec())
    {
        user=auth2.data();
    }
    else
    {
        return;
    }
}
    user_=storage_->getUserAuth(user.username,user.hash_password).value();
    ui->add_button->setEnabled(true);
    //ui->label_2->setText(user_.fullname);
    vector<capital> cats=storage_->getAllUserCountry(user_.id);
    this->addCats(cats);

}

void MainWindow::on_add_new_clicked()
{
    AddDialogGDP add(this);
    int status=add.exec();
    if(status==1)
    {
        GDP country=add.data();
        //country.user_id=user_.id;
        country.id=storage_->insertGDP(country);
        qDebug()<<country.id;
        QVariant qVar=QVariant::fromValue(country);
        QListWidgetItem * qCatListItem=new QListWidgetItem();
        qCatListItem->setText(QString::number(country.unemp));
        qCatListItem->setData(Qt::UserRole,qVar);
        ui->listWidget_2->addItem(qCatListItem);
    }
    else
    {

    }
}
void MainWindow::enter()
{
    regORlog rl;
    if(rl.exec())
    {
        if(rl.data())
        {
//        User user;
//        Auth auth(this);
//        if
            User user;
           Auth auth(this);
           //regORlog a(this);

            if(auth.exec())
           {
                user=auth.data();
            }
           else
           {
               qDebug()<<"1";
              return;
           }
           //qDebug()<<user.username<<user.hash_password;
           const User u=user;

           while (storage_->getUserAuth(user.username,user.hash_password)==nullopt)
           {
               qDebug()<<user.username;
               QMessageBox::information(this,"Authentication","Incorrect login or password. Try again");
               Auth auth2(this);
               if(auth2.exec())
               {
                   user=auth2.data();
               }
               else
               {
                   qDebug()<<"2";
                   return;
               }
           }

           user_=storage_->getUserAuth(user.username,user.hash_password).value();
           ui->add_button->setEnabled(true);
           //ui->label_2->setText(user_.fullname);
           //qDebug()<<fileName;
           vector<capital> b=storage_->getAllUserCountry(user_.id);
           qDebug()<<"Size: "<<b.size()<<"User:"<<user_.id;
           this->addCats(b);

        }
        if(!rl.data())
        {
            Dialog add(this);
             User user;
            if(add.exec())
           {
                user=add.data();
            }
           else
           {
               qDebug()<<"1";
              return;
           }
           //qDebug()<<user.username<<user.hash_password;
           const User u=user;

           while (user.username.size()<=5&&user.hash_password.size()<=7)
           {
               qDebug()<<user.username;
               QMessageBox::information(this,"Authentication","password must be more than 6 characters\n username must be more than 4 characters\n");
               Dialog auth2(this);
               if(auth2.exec())
               {
                   user=auth2.data();
               }
               else
               {
                   qDebug()<<"2";
                   return;
               }
           }



                user.id=storage_->insertUser(user);
                QVariant qVar=QVariant::fromValue(user);
                QListWidgetItem * qCatListItem=new QListWidgetItem();




        }
    }
}

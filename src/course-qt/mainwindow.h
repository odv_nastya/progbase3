#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "storage.h"
#include"user.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    User user_;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addCats(const vector<capital> & cats);
    void addBreeds(const vector<GDP> & breeds);
private slots:
    void onOpen();

    void onLogout();
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_remove_button_clicked();

    void on_add_button_clicked();

    void on_edit_button_clicked();
    void beforeClose();
    void on_listWidget_2_itemClicked(QListWidgetItem *item);

    void on_add_button_2_clicked();

    void on_remove_button_2_clicked();

    void on_add_new_clicked();
    void enter();

private:
    Ui::MainWindow *ui;
    Storage * storage_=nullptr;
};

#endif // MAINWINDOW_H
